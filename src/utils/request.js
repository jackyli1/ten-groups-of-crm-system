import axios from 'axios'
import { getToken } from './auth'
import { Message } from 'element-ui'
const service = axios.create({
  baseURL: 'http://192.168.179.32:8080',
  // baseURL: 'http://192.168.172.115:8080',
  // baseURL: 'http://192.168.172.35:8080',
  timeout: 5000 // request timeout
  // timeout: 10000 // request timeout
})

// 请求拦截器
service.interceptors.request.use(
  config => {
    const token = getToken()
    if (token) {
      config.headers.token = token
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  response => {
    return response.data
  },
  error => {
    // 错误统一处理
    Message.error('操作失败')
    return Promise.reject(error)
  }
)

export default service
