export function tranListToTreeData(list, rootValue) {
  const arr = []
  list.forEach(item => {
    if (item.parentId === rootValue) {
      // 以 item.id 作为 父 id, 接着往下找
      const children = tranListToTreeData(list, item.menuId)
      if (children.length > 0) {
        item.children = children
      }
      // 将item项, 追加到arr数组中
      arr.push(item)
    }
  })
  return arr
}
