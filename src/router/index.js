import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const routes = [
  {
    path: '/login',
    component: () => import('@/views/Login/index'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/workbench'
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/workbench',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/Workbench/index'),
      meta: { title: '首页', icon: 'el-icon-house' }
    }]
  },
  {
    path: '/clueManagement',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/clueManagement/index'),
      meta: { title: '线索管理', icon: 'el-icon-add-location' }
    }]
  },
  // 线索页查看详情
  {
    path: '/lookClue',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/lookClue/index'),
      hidden: true
    }]
  },
  // 商机管理
  {
    path: '/businessOpportunity',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/businessOpportunity/index'),
      meta: { title: '商机管理', icon: 'el-icon-tableware' }
    }]
  },
  {
    path: '/followup',
    component: Layout,
    hidden: true,
    children: [{
      path: '',
      component: () => import('@/views/businessOpportunity/components/FollowUp'),
      meta: { title: '商机更进' }
    }]
  },
  {
    path: '/contractManagement',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/contractManagement/index'),
      meta: { title: '合同管理', icon: 'el-icon-tickets' }
    }]
  },
  // 合同详情的路由
  {
    path: '/contractExamine',
    component: () => import('@/views/contractManagement/contractExamine')
  },

  {
    path: '/redeploy',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/redeploy/index'),
      meta: { title: '转派管理', icon: 'el-icon-sort' }
    }]
  },
  {
    path: '/basicData',
    component: Layout,
    permission: 'basicData',
    meta: { title: '基础数据维护', icon: 'el-icon-office-building' },
    children: [{
      path: 'Course',
      permission: 'basicData:Course',
      meta: { title: '课程管理' },
      component: () => import('@/views/basicData/Course/index')
    },
    {
      path: 'Activety',
      permission: 'basicData:Activety',
      meta: { title: '活动管理' },
      component: () => import('@/views/basicData/Activety/index')
    }]
  },
  {
    path: '/StatisticalAnalysis',
    component: Layout,
    hidden: true,
    children: [{
      path: '',
      component: () => import('@/views/StatisticalAnalysis/index'),
      meta: { title: '统计分析', icon: 'el-icon-data-board' }
    }]
  },
  {
    path: '/SystemManagement',
    component: Layout,
    meta: { title: '系统管理', icon: 'el-icon-setting' },
    children: [
      {
        path: 'NotificationCenter',
        component: () => import('@/views/SystemManagement/NotificationCenter'),
        meta: { title: '通知中心' }
      },
      {
        path: 'AuthorityManagement', // 三级路由 记得在二级路由的index页面中配置路由出口
        meta: { title: '权限管理' },
        component: () => import('@/views/SystemManagement/AuthorityManagement'),
        children: [
          {
            path: 'user',
            component: () => import('@/views/SystemManagement/AuthorityManagement/User'),
            meta: { title: '用户管理' },
            hidden: true
          },
          {
            path: 'role',
            component: () => import('@/views/SystemManagement/AuthorityManagement/Role'),
            meta: { title: '角色管理', icon: 'el-icon-user' }
          },
          {
            path: 'Menu',
            component: () => import('@/views/SystemManagement/AuthorityManagement/Menu'),
            meta: { title: '菜单管理', icon: 'el-icon-eleme' }
          },
          {
            path: 'Department',
            component: () => import('@/views/SystemManagement/AuthorityManagement/Department'),
            meta: { title: '部门管理', icon: 'el-icon-s-unfold' }
          },
          {
            path: 'Position',
            component: () => import('@/views/SystemManagement/AuthorityManagement/Position'),
            meta: { title: '岗位管理', icon: 'el-icon-s-check' }
          }
        ]
      }
    ]
  },
  {
    path: '/dictionary',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/Dictionary/index'),
      meta: { title: '字典管理', icon: 'el-icon-food' }
    }]
  },
  {
    path: '/ThreadAllocation',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/ThreadAllocation/index'),
      meta: { title: '线索配置', icon: 'el-icon-chicken' }
    }]
  },
  {
    path: '/BusinessAllocation',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/BusinessAllocation/index'),
      meta: { title: '商机配置', icon: 'el-icon-fork-spoon' }
    }]
  },
  {
    path: '/Logs',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/Logs/index'),
      meta: { title: '系统日志', icon: 'el-icon-goblet-square-full' }
    }]
  },
  {
    path: '/quitSystem',
    component: Layout,
    children: [{
      path: '',
      component: () => import('@/views/quitSystem/index'),
      meta: { title: '退出系统', icon: 'el-icon-cherry' }
    }]
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: routes
})

const router = createRouter()

// 重置路由方法
export function resetRouter() {
  // 得到一个全新的router实例对象
  const newRouter = createRouter()
  // 使用新的路由记录覆盖掉老的路由记录
  router.matcher = newRouter.matcher
}

export default router
