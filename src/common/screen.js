export function removeLastChildren(children) {
  children.forEach(function(child) {
    if (child.children.length > 0) {
      removeLastChildren(child.children)
    } else {
      delete child.children
    }
  })
}
