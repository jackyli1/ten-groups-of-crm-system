import request from '@/utils/request'
/**
 * 请求列表
 * @param {*} params
 * @returns
 */
export const getClueListApi = (params) => {
  return request({
    url: '/crm/clues/clue/list',
    params
  })
}
/**
 * 新增线索
 * @param {*} data
 * @returns
 */
export const addClueApi = (data) => {
  return request({
    url: '/crm/clues/clue/addclue',
    method: 'post',
    data
  })
}
/**
 * 获取活动列表
 * @returns
 */
export const getactivityListApi = () => {
  return request({
    url: '/crm/clues/clue/activityList'
  })
}
/**
 * 批量捞取线索
 * @param {*} data
 * @returns
 */
export const largeClueApi = (data) => {
  return request({
    url: '/crm/clues/clue/gainclues',
    method: 'psot',
    data
  })
}
/**
 *  获取某行详情ID
 * @param {*} params
 * @returns
 */
export const detailClueApi = (params) => {
  return request({
    url: '/crm/clues/clue/getTbClue',
    params
  })
}
/**
 * 获取分配弹窗里的部门列表
 * @returns
 */
export const getDepartmentApi = () => {
  return request({
    url: '/crm/clues/clue/deptList'
  })
}
/**
 *  根据部门ID获取员工列表
 * @param {*} params
 * @returns
 */
export const getDepartmentStaffApi = (params) => {
  return request({
    url: '/crm/clues/clue/getUserListByDept',
    params
  })
}
/**
 * 批量分配的接口
 * @param {*} data
 * @returns
 */
export const addfenpeiClueApi = (data) => {
  return request({
    url: '/crm/clues/clue/allotClues',
    method: 'post',
    data
  })
}
/**
 * 修改 之后点击确定上传
 * @param {*} data
 * @returns
 */
export const putClueApi = (data) => {
  return request({
    url: '/crm/clues/clue/addFollowUp',
    method: 'put',
    data
  })
}
/**
 * 转商机接口
 * @param {*} data
 * @returns
 */
export const clueToBusinessApi = (data) => {
  return request({
    url: '/crm/clues/clue/clueToBusiness',
    method: 'put',
    data
  })
}
/**
 * 弹窗确认转 伪线索
 * @param {*} data
 * @returns
 */
export const clueTofalseApi = (data) => {
  return request({
    url: '/crm/clues/clue/falseClue',
    method: 'put',
    data
  })
}
/**
 * 获取意向学科列表
 * @returns
 */
export const subjectLikeApi = () => {
  return request({
    url: '/api/system/dict/data/type/course_subject'
  })
}
/**
 *  获取意向等级列表
 * @returns
 */
export const subjectlevelApi = () => {
  return request({
    url: '/api/system/dict/data/type/clues_level'
  })
}
// 伪线索上报原因
export const reasonForfalseApi = () => {
  return request({
    url: '/api/system/dict/data/type/reasons_for_reporting'
  })
}
//* ****************************查询线索池列表 */
/**
 * 获取线索池列表信息
 * @returns
 */
export const getCluePoolApi = (params) => {
  return request({
    url: '/crm/clues/clue/pool',
    params
  })
}
/**
 * 批量捞取
 * @param {*} data
 * @returns
 */
export const largeLaoquApi = (data) => {
  return request({
    url: '/crm/clues/clue/gainclues',
    method: 'post',
    data
  })
}

