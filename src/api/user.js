import request from '@/utils/request'

/**
 * 登录
 * @param {*} data
 * @returns
 */
export function loginAPI(data) {
  return request.post('/api/login', data)
}

/**
 * 获取验证码
 * @returns
 */
export function getCaptchaApi() {
  return request('/api/getCaptcha')
}
