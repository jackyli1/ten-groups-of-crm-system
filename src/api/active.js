import request from '@/utils/request'

/**
 * 获取渠道来源下拉列表
 * @returns
 */
// url: 'crm/activities/typeList'  channelList
export const getchannelListApi = () => {
  return request({
    url: 'crm/activities/typeList'
  })
}

/**
 * 获取列表
 * @param {*} params
 * @returns
 */
export const getActiveListApi = (params) => {
  return request({
    url: '/crm/activities/page',
    params
  })
}

/**
 * 添加活动
 * @param {*} data
 * @returns
 */
export const addActiveApi = (data) => {
  return request({
    url: '/crm/activities',
    method: 'POST',
    data
  })
}

/**
 * 删除活动
 * @param {*} id
 * @returns
 */
export const removeActiveApi = (id) => {
  return request({
    url: `/crm/activities/${id}`,
    method: 'DELETE'
  })
}

/**
 * 修改时回显
 * @param {*} data
 * @returns
 */
export const getActiveApi = (id) => {
  return request({
    url: `/crm/activities/${id}`
  })
}

/**
 * 修改活动
 * @param {*} data
 * @returns
 */
export const editActiveApi = (data) => {
  return request({
    url: '/crm/activities',
    method: 'PUT',
    data
  })
}
