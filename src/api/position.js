import request from '@/utils/request'

export function getPositionAPI(params) {
  return request({
    url: '/crm/post/posts/page',
    params
  })
}

/**
 * 刪除
 */
export function delPositionAPI(names) {
  return request({
    url: `/crm/post/posts/delete?names=${names}`,
    method: 'delete'
  })
}

/**
 * 新增
 */
export function addPositionAPI(form) {
  return request({
    url: '/crm/post/posts/save',
    method: 'post',
    data: form

  })
}
/**
 * 批量删除
 * @param {*} postids
 * @returns
 */
export function delAllAPI(postids) {
  return request({
    url: `/crm/post/posts/deletes?postids=${postids}`,
    method: 'delete'
  })
}
/**
 * 数据回显
 */
export function getPosiApi(postId) {
  return request({
    url: `/crm/post/posts/${postId}`
  })
}

/**
 * 编辑
 */
export function editApi(data) {
  return request({
    url: '/crm/post/posts/update',
    method: 'put',
    data

  })
}
/**
 * 导出
 */
export function handleDownloadAPI() {
  return request({
    url: '/crm/post/posts/export'
  })
}
