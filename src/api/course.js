import request from '@/utils/request'
/**
 * 课程列表
 */

export function getCourseListApi(params) {
  return request({
    url: '/crm/courses/page',
    params
  })
}
/**
 * 批量删除
 * @param {*} ids
 * @returns
 */
export function delCourseApi(ids) {
  return request({
    url: `/crm/courses?ids=${ids.join(',')}`,
    method: 'delete'
  })
}
/**
 * 单个删除
 * @param {*} id
 * @returns
 */
export function delApi(id) {
  return request({
    url: `/crm/courses/${id}`,
    method: 'delete'
  })
}
/**
 * 添加学科下拉框
 * @returns
 */
export function subjectListAPI() {
  return request({
    url: '/crm/courses/subjectList'
  })
}
/**
 * 添加人群下拉框
 */
export function applicablePersonListAPI() {
  return request({
    url: '/crm/courses/applicablePersonList'
  })
}

/**
 * 添加
 */
export function addCourseAPI(data) {
  return request({
    url: '/crm/courses',
    method: 'post',
    data
  })
}

/**
 * 编辑
 */
export function editAPI(data) {
  return request({
    url: '/crm/courses',
    method: 'put',
    data
  })
}
