import request from '@/utils/request'

/**
 * 渲染列表
 */
export function getDictionaryAPI(params) {
  return request({
    url: '/sys/dict/type/page',
    params
  })
}

/**
 * 删除
 */
export function delDictionaryAPI(ids) {
  return request({
    url: `/sys/dict/type?ids=${ids}`,
    method: 'delete'
  })
}

/**
 * 添加
 */

export function addDictionaryAPI(data) {
  return request({
    url: '/sys/dict/type/save',
    method: 'post',
    data
  })
}

/**
 * 编辑
 */
export function editDictionaryAPI(data) {
  return request({
    url: '/sys/dict/type/update',
    method: 'put',
    data
  })
}

/**
 * 导出
 */
export function handleDwonload() {
  return request({
    url: '/sys/dict/type/export'
  })
}

/*
 * 获取职业列表
 * @returns
 */
export function getOccupationListApi() {
  return request('/api/system/dict/data/type/occupation')
}

/**
 * 获取学历列表
 * @returns
 */
export function getEducationalListApi() {
  return request('/api/system/dict/data/type/education')
}

/**
 * 获取专业列表
 * @returns
 */
export function getMajorListApi() {
  return request('/api/system/dict/data/type/major')
}

/**
* 获取在职情况
 * @returns
 */
export function getJobListApi() {
  return request('/api/system/dict/data/type/job')
}

/**
 * 获取薪资
 * @returns
 */
export function getSalaryListApi() {
  return request('/api/system/dict/data/type/salary')
}

/**
 * 获取意向学科
 * @returns
 */
export function getSubjectlListApi() {
  return request('/api/system/dict/data/type/course_subject')
}

/**
 * 获取意向课程
 * @returns
 */
export function getCourseListApi() {
  return request('/crm/courses/page?pageNum=1&pageSize=30')
}

/**
 * 获取跟进状态
 * @returns
 */
export function getTrackStatusApi() {
  return request('/api/system/dict/data/type/track_status')
}

/**
 * 获取沟通重点
 * @returns
 */
export function getCommunicationPointApi() {
  return request('/api/system/dict/data/type/communication_point')
}

/**
 * 获取不感兴趣原因
 * @returns
 */
export function getReportingApi() {
  return request('/api/system/dict/data/type/reasons_for_business_reporting')
}
