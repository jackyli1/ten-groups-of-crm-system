import request from '@/utils/request'
/**
 * 获取菜单管理列表
 */
export const getMenuAPI = (params) => {
  return request({
    url: '/crm/menus/menu/page',
    params
  })
}
/**
 * 删除菜单
 */
export const delMenuAPI = (names) => {
  return request({
    url: `/crm/menus/menu/delete?names=${names}`,
    method: 'DELETE'
  })
}
/**
 * 添加菜单
 */
export const addMenuAPI = (data) => {
  return request({
    url: '/crm/menus/menu/save',
    method: 'POST',
    data
  })
}
/**
 * 回显菜单
 */
export const disPlayAPI = (menuId) => {
  return request({
    url: `/crm/menus/menu/${menuId}`
  })
}
/**
 * 编辑菜单
 */
export const editAPI = (data) => {
  return request({
    url: '/crm/menus/menu/update',
    method: 'PUT',
    data
  })
}
