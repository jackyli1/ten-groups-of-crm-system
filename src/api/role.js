import request from '@/utils/request'
/**
 * 获取角色列表
 * @returns
 */
export const getRoleListApi = (params) => {
  return request({
    url: '/role/page',
    params
  })
}
/**
 * 删除角色
 * @param {*} roleIds
 * @returns
 */
export const cancelRoleListApi = (roleId) => {
  return request({
    url: `/role?roleIds=${roleId}`,
    method: 'DELETE'
  })
}
/**
 * 确认修改
 * @param {*} data
 * @returns
 */
export const editRoleListApi = (data) => {
  return request({
    url: `/role`,
    method: 'PUT',
    data
  })
}
/**
 * 回显
 * @param {*} roleId
 * @returns
 */
export const detailRoleListApi = (roleId) => {
  return request({
    url: `/role/${roleId}`
  })
}/**
 * 添加角色
 * @param {*} data
 * @returns
 */
export const addRoleListApi = (data) => {
  return request({
    url: '/role',
    method: 'POST',
    data
  })
}
/**
 * 修改角色状态
 * @returns
 */
export const editStatusApi = () => {
  return request({
    url: '/role/status'
  })
}
