import request from '@/utils/request'
// 员工信息回显
export function getTreeSelectListApi() {
  return request({
    url: '/system/user/treeselect'
  })
}
// 转派分页查询

export function getListApi(params) {
  return request({
    url: '/transfer/list',
    params
  })
}

// 部门回显
export function getDetailApi(deptid) {
  return request({
    url: `/system/dept/listselect`
  })
}

// 修改部门信息
export function putChangeApi(data) {
  return request({
    url: '/system/dept/treeselect',
    method: 'put',
    data
  })
}
