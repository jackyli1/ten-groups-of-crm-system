import request from '@/utils/request'

/**
 * 获取部门列表
 * @param {*} params
 * @returns
 */
export const getDeptApi = (params) => {
  return request({
    url: '/dept/list',
    params
  })
}

/**
 * 新增部门
 * @param {*} data
 * @returns
 */
export const addDeptApi = (data) => {
  return request({
    url: '/dept',
    method: 'POST',
    data
  })
}

/**
 * 删除部门
 * @param {*} id
 * @returns
 */
export const removeDeptApi = (deptId) => {
  return request({
    url: `/dept?deptId=${deptId}`,
    method: 'DELETE'
  })
}

/**
 * 回显部门
 * @param {*} id
 * @returns
 */
export const editDeptApi = (id) => {
  return request({
    url: `/dept/${id}`
  })
}

/**
 * 编辑部门
 * @param {*} data
 * @returns
 */
export const modifyDeptApi = (data) => {
  return request({
    url: '/dept',
    method: 'PUT',
    data
  })
}
