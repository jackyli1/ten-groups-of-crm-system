import request from '@/utils/request'

/**
 * 获取商机列表
 * @param {*} params
 * @returns
 */
export function getBusinessListApi(params) {
  return request('/crm/business/list', { params })
}

/**
 * 添加商机
 * @param {*} data
 * @returns
 */
export function addBusiness(data) {
  return request.post('/crm/business/save', data)
}

/**
 * 获取部门列表
 * @returns
 */
export function getDeptListApi() {
  return request('/crm/clues/clue/deptList')
}

/**
 * 查看商机详情
 * @param {*} id
 * @returns
 */
export function viewBusinessApi(id) {
  return request(`/crm/business/${id}`)
}

/**
 * 跟进商机
 * @param {*} id
 * @returns
 */
export function getFollowUpBusinessApi(id) {
  return request(`/crm/business/follow/${id}`)
}

/**
 * 跟进商机详情
 * @param {*} id
 * @returns
 */
export function getBusinessDescApi(id) {
  return request('/crm/business/getBusinessDesc', { params: { id }})
}

/**
 * 修改跟进商机信息
 * @param {*} data
 * @returns
 */
export function editBusinessFollowUpApi(data) {
  return request.put('/crm/business/businessFollowUp', data)
}

/**
 * 商机_跟进_踢回公海
 * @param {*} data
 * @returns
 */
export function editBusinessReturnPublicApi(data) {
  return request.put('/crm/business/businessReturnPublic', data)
}

/**
 * 获取公海列表
 * @returns
 */
export function getBusinessPoolApi() {
  return request('/crm/business/pool')
}
