import request from '@/utils/request'
/**
 * 客户统计
 * @param {*} params
 * @returns
 */
export const getCustomerApi = (params) => {
  return request({
    url: '/crm/StatisticAnalysis/client/newClient',
    params
  })
}
