import request from '@/utils/request'
/**
 * 获取合同列表
 * @returns
 */
export const getContractListAPI = (params) => {
  return request({
    url: '/contract/list',
    params
  })
}

/**
 * 添加合同
 * @param {*} data
 * @returns
 */
export const addContractApi = (data) => {
  return request({
    url: `/clues/course/listselect?name=${data.name}&phone=${data.phone}&fileName=${data.fileName}`,
    method: 'POST'
  })
}
/**
 * 合同详情
 * @returns
 */
export const detailContractApi = (id) => {
  return request({
    url: `/contract/${id}`
  })
}
/**
 * 修改合同
 * @returns
 */
export const editRoleListApi = (data) => {
  return request({
    url: `/clues/course/listselect`,
    method: 'PUT',
    data
  })
}
/**
 * 文件上传
 * @param {*} data
 * @returns
 */
export const uploadApi = (data) => {
  return request({
    url: '/common/upload',
    method: 'POST',
    data
  })
}
