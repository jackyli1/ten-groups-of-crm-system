const getters = {
//   sidebar: state => state.app.sidebar,
//   device: state => state.app.device,
//  为了少写两个变量名 直接写 store.getters.token即可
//  可以在main.js里面打印一下store上面都有这些方法和属性
  token: state => state.user.token,
  //  不是很懂
  //   name: state => state.user.userInfo.username,
  //   staffPhoto: state => state.user.userInfo.staffPhoto
  //   staffPhoto: state => state.user.userInfo.staffPhoto,
  routes: state => state.routes.routes
//   roles: state => state.user.userInfo.roles
}
export default getters
