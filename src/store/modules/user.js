import { loginAPI } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { TOKEN_KEY } from '@/constants/KEY'
export default {
  namespaced: true,
  state: () => {
    return {
      token: getToken() || ''
    }
  },
  mutations: {
    setToken(state, token) {
      state.token = token
      setToken(token)
    },
    clearUserInfo(state) {
      // 清除Token
      state.token = ''
      removeToken(TOKEN_KEY)
    }
  },
  actions: {
    async submit(ctx, data) {
      const res = await loginAPI(data)
      // console.log(res)
      ctx.commit('setToken', res.data.token)
    }
  }
}
