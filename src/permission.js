// 权限控制
import router from './router'
import { getToken } from './utils/auth'

const WHITE_LIST = ['/login/', '/404/']
router.beforeEach((to, from, next) => {
  const token = getToken()
  // 有token
  if (token) {
    if (to.path === WHITE_LIST[0]) {
      next('/workbench')
    } else {
      next()
    }
  } else {
    // 没有token
    if (WHITE_LIST.includes(to.path)) {
      next()
    } else {
      next('/login/')
    }
  }
})
